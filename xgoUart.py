from machine import UART,Pin

uart = UART(2, baudrate=115200, rx=13,tx=12,timeout=10)
print("Uart set ok!")

def uart_send(msg):
    uart.write(msg)

def uart_add(msg):
    wholemsg = []
    wholemsg.append(msg)
    uart_send(bytes(wholemsg))

def addMessage(msg):
    length = len(msg) + 7
    order = 0x00
    message = msg
    tempLength = int(length)
    tempSum = tempLength + int(order)
    for a in message:
        tempSum = tempSum + int(a)
    if tempSum > 255:
        tempSum = tempSum % 256
    checksum = 255 - tempSum
    wholemsg = []
    wholemsg.append(0x55)
    wholemsg.append(0x00)
    wholemsg.append(length)
    wholemsg.append(order)
    for i in range(0,len(message)):
        wholemsg.append(message[i])
    wholemsg.append(checksum)
    wholemsg.append(0x00)
    wholemsg.append(0xAA)
    uart_send(bytes(wholemsg))


def addMessageRespond(msg):
    length = len(msg) + 7
    order = 0x01
    message = msg
    tempLength = int(length)
    tempSum = tempLength + int(order)
    for a in message:
        tempSum = tempSum + int(a)
    if tempSum > 255:
        tempSum = tempSum % 256
    checksum = 255 - tempSum
    wholemsg = []
    wholemsg.append(0x55)
    wholemsg.append(0x00)
    wholemsg.append(length)
    wholemsg.append(order)
    for i in range(0,len(message)):
        wholemsg.append(message[i])
    wholemsg.append(checksum)
    wholemsg.append(0x00)
    wholemsg.append(0xAA)
    uart_send(bytes(wholemsg))

def addMessageRead(msg):
    length = len(msg) + 7
    order = 0x02
    message = msg
    tempLength = int(length)
    tempSum = tempLength + int(order)
    for a in message:
        tempSum = tempSum + int(a)
    if tempSum > 255:
        tempSum = tempSum % 256
    checksum = 255 - tempSum
    wholemsg = []
    wholemsg.append(0x55)
    wholemsg.append(0x00)
    wholemsg.append(length)
    wholemsg.append(order)
    for i in range(0,len(message)):
        wholemsg.append(message[i])
    wholemsg.append(checksum)
    wholemsg.append(0x00)
    wholemsg.append(0xAA)
    uart_send(bytes(wholemsg))


