from machine import Pin
from neopixel import NeoPixel
import time 

def find_dog_rgb(pin,offset,red,green,bule):
    
    pin = Pin(pin,Pin.OUT)
    np = NeoPixel(pin,8)
    np[offset] = (red,green,bule)
    np.write()

