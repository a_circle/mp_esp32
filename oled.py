from machine import Pin, I2C
from ssd1306 import SSD1306_I2C

i2c = I2C(scl=Pin(5), sda=Pin(4))

oled = SSD1306_I2C(128, 64, i2c)

def find_dog_oled(x,y,level):
    oled.pixel(x, y, level)
    oled.show()
    
find_dog_oled(43,54,0)