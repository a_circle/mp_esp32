import time
from xgoUart import addMessage
from xgoUart import addMessageRespond
from xgoUart import addMessageRead


def gyr_set(X,Y=128):
    if X==0:
        msg = [0x36,Y]
        addMessage(msg)
    elif X==1:
        msg = [0x37,Y]
        addMessage(msg)
    elif X==2:
        msg = [0x38,Y]
        addMessage(msg)
def motorRun(X):
    if X==0:
        msg = [0x5C,30]
        addMessage(msg)
    elif X==1:
        msg = [0x5C,150]
        addMessage(msg)
    elif X==2:
        msg = [0x5C,255]
        addMessage(msg)
def servo(X,Y):
    if X==11:
        msg = [0x50,Y]
        addMessage(msg)
    elif X==12:
        msg = [0x51,Y]
        addMessage(msg)
    elif X==13:
        msg = [0x52,Y]
        addMessage(msg)
    elif X==21:
        msg = [0x53,Y]
        addMessage(msg)
    elif X==22:
        msg = [0x54,Y]
        addMessage(msg)
    elif X==23:
        msg = [0x55,Y]
        addMessage(msg)
    elif X==31:
        msg = [0x56,Y]
        addMessage(msg)
    elif X==32:
        msg = [0x57,Y]
        addMessage(msg)
    elif X==33:
        msg = [0x58,Y]
        addMessage(msg)
    elif X==41:
        msg = [0x59,Y]
        addMessage(msg)
    elif X==42:
        msg = [0x5A,Y]
        addMessage(msg)
    elif X==43:
        msg = [0x5B,Y]
        addMessage(msg)
        
def move(N,Y,X):
    if Y==0:
        msg = [0x3D,1]
        addMessage(msg)
    elif Y==1:
        msg = [0x3D,0]
        addMessage(msg)
    elif Y==2:
        msg = [0x3D,2]
        addMessage(msg)
    
    if N==0:
        msg = [0x30,200]
        addMessage(msg)
    elif N==1:
        msg = [0x30,55]
        addMessage(msg)
    elif N==2:
        msg = [0x32,200]
        addMessage(msg)
    elif N==3:
        msg = [0x32,55]
        addMessage(msg)
    elif N==4:
        msg = [0x31,200]
        addMessage(msg)
    elif N==5:
        msg = [0x31,55]
        addMessage(msg)
    
    time.sleep(X)

    if N==0:
        msg = [0x30,128]
        addMessage(msg)
    elif N==1:
        msg = [0x30,128]
        addMessage(msg)
    elif N==2:
        msg = [0x32,128]
        addMessage(msg)
    elif N==3:
        msg = [0x32,128]
        addMessage(msg)
    elif N==4:
        msg = [0x31,128]
        addMessage(msg)
    elif N==5:
        msg = [0x31,128]
        addMessage(msg)
        
        

def pose_set(X):
    if X==0:
        msg = [0x35,50]
        addMessage(msg)
    elif X==1:
        msg = [0x35,150]
        addMessage(msg)
    elif X==2:
        msg = [0x35,255]
        addMessage(msg)

def stable_on():
    msg = [0x61,1]
    addMessage(msg)

def stable_off():
    msg = [0x61,0]
    addMessage(msg)

def reset():
    msg = [0x5D,1]
    addMessage(msg)



