from machine import UART,Pin

uart = UART(2, baudrate=115200, rx=13,tx=12,timeout=10)
print("Uart set ok!")

def uart_send(msg):
  uart.write(msg.encode('utf-8'))

def go_stop():
  uart_send("\x00")
  print("Xgo stop!")
def go_forward():
  uart_send("\x01")
  print("Xgo go forward!")
def go_back():
  uart_send("\x02")
  print("Xgo go back!")
def go_left():
  uart_send("\x03")
  print("Xgo go back!")
def go_right():
  uart_send("\x04")
  print("Xgo go back!")
def go_clockwise():
  uart_send("\x05")
  print("Xgo go back!")
def go_anticlockwise():
  uart_send("\x06")
  print("Xgo go back!")
def go_roll():
  uart_send("\x07")
  print("Xgo go back!")
def go_pitch():
  uart_send("\x08")
  print("Xgo go back!")
def go_yaw():
  uart_send("\x09")
  print("Xgo go back!")
def go_higher():
  uart_send("\x0A")
  print("Xgo go back!")
def go_lower():
  uart_send("\x0B")
  print("Xgo go back!")
def go_IMUon():
  uart_send("\x0C")
  print("Xgo go back!")
def go_IMUoff():
  uart_send("\x0D")
  print("Xgo go back!")
def go_getdown():
  uart_send("\x33")
  print("Xgo go back!")
def go_standup():
  uart_send("\x34")
  print("Xgo go back!")
def go_crawling():
  uart_send("\x35")
  print("Xgo go back!")
def go_trunaround():
  uart_send("\x36")
  print("Xgo go back!")
def go_step():
  uart_send("\x37")
  print("Xgo go back!")
def go_squatup():
  uart_send("\x38")
  print("Xgo go back!")
def go_roll_once():
  uart_send("\x39")
  print("Xgo go back!")
def go_pitch_once():
  uart_send("\x3A")
  print("Xgo go back!")
def go_yaw_once():
  uart_send("\x3B")
  print("Xgo go back!")
def go_axis_once():
  uart_send("\x3C")
  print("Xgo go back!")
def go_pee():
  uart_send("\x3D")
  print("Xgo go back!")
def go_sitdown():
  uart_send("\x3E")
  print("Xgo go back!")
def go_wavehand():
  uart_send("\x3F")
  print("Xgo go back!")
def go_stretch():
  uart_send("\x40")
  print("Xgo go back!")
def go_waves():
  uart_send("\x41")
  print("Xgo go back!")
def go_swing():
  uart_send("\x42")
  print("Xgo go back!")
def go_askforfood():
  uart_send("\x43")
  print("Xgo go back!")
def go_seekfood():
  uart_send("\x44")
  print("Xgo go back!")
def go_handshake():
  uart_send("\x45")
  print("Xgo go back!")

  